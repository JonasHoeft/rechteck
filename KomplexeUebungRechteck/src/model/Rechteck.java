package model;

public class Rechteck {
private int x = 0;
private int y = 0;
private int breite = 0;
private int hoehe = 0;

public Rechteck() {
	super();
}

public Rechteck(int x, int y, int breite, int hoehe) {
	super();
	this.x = x;
	this.y = y;
	this.breite = breite;
	this.hoehe = hoehe;
}

public int getX() {
	return x;
}

public void setX(int x) {
	this.x = x;
}

public int getY() {
	return y;
}

public void setY(int y) {
	this.y = y;
}

public int getBreite() {
	return breite;
}

public void setBreite(int breite) {
	this.breite = breite;
}

public int getHoehe() {
	return hoehe;
}

public void setHoehe(int hoehe) {
	this.hoehe = hoehe;
}

@Override
public String toString() {
	return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + "]";
}



}
