package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {
	Rechteck r00 = new Rechteck(10,10,30,40);
	Rechteck r01 = new Rechteck(25,25,100,20);
	Rechteck r02 = new Rechteck(260,10,200,100);
	Rechteck r03 = new Rechteck(5,500,300,25);
	Rechteck r04 = new Rechteck(100,100,100,100);
	Rechteck r05 = new Rechteck();
	Rechteck r06 = new Rechteck();
	Rechteck r07 = new Rechteck();
	Rechteck r08 = new Rechteck();
	Rechteck r09 = new Rechteck();

	r05.setX(200);
	r05.setY(200);
	r05.setBreite(200);
	r05.setHoehe(200);
	
	r06.setX(800);
	r06.setY(400);
	r06.setBreite(20);
	r06.setHoehe(20);
	
	r07.setX(800);
	r07.setY(450);
	r07.setBreite(20);
	r07.setHoehe(20);
	
	r08.setX(850);
	r08.setY(400);
	r08.setBreite(20);
	r08.setHoehe(20);
	
	r09.setX(855);
	r09.setY(455);
	r09.setBreite(25);
	r09.setHoehe(25);
	
	System.out.println(r00.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));
	

 BunteRechteckeController cnt = new BunteRechteckeController();
 cnt.addRechteck(r00);
 cnt.addRechteck(r01);
 cnt.addRechteck(r02);
 cnt.addRechteck(r03);
 cnt.addRechteck(r04);
 cnt.addRechteck(r05);
 cnt.addRechteck(r06);
 cnt.addRechteck(r07);
 cnt.addRechteck(r08);
 cnt.addRechteck(r09);
 
 System.out.println(cnt.toString().equals("BunteRechteckeController [rechtecke=[Rechteck [x=10, y=10, breite=30, hoehe=40], Rechteck [x=25, y=25, breite=100, hoehe=20], Rechteck [x=260, y=10, breite=200, hoehe=100], Rechteck [x=5, y=500, breite=300, hoehe=25], Rechteck [x=100, y=100, breite=100, hoehe=100], Rechteck [x=200, y=200, breite=200, hoehe=200], Rechteck [x=800, y=400, breite=20, hoehe=20], Rechteck [x=800, y=450, breite=20, hoehe=20], Rechteck [x=850, y=400, breite=20, hoehe=20], Rechteck [x=855, y=455, breite=25, hoehe=25]]]"
		 ));
 
 
	}	

}
