package controller;

import java.util.LinkedList;
import java.util.List;

import model.Rechteck;


public class BunteRechteckeController {
List<Rechteck> rechtecke = new LinkedList<Rechteck>();

public BunteRechteckeController() {
rechtecke.clear();

}

public List<Rechteck> getRechtecke() {
	return rechtecke;
}

public Rechteck getRechteck(int pos) {
	return rechtecke.get(pos);
}

public void  addRechteck(Rechteck rechteck) {
	rechtecke.add(rechteck);
}

public void reset() {
	rechtecke.clear();

}

@Override
public String toString() {
	return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
}




}
